## Mgcs_AdminStartup

Adds the ability to specify a per user startup page for the backend. It overrules
the default setting under *System | Configuration | Advanced | Admin*.

![mgcs_adminstartup_01.png](https://bitbucket.org/repo/AzG4Mq/images/962030655-mgcs_adminstartup_01.png)

## The problem
The problem I had was that some of the backend users didn't have the right permissions
to access the configured startup page (by default this is the Dashboard). By default
these users get redirected to the first accessible menu item (see `Mage_Admin_Model_User::findFirstAvailableMenu()`).

## The solution
The easiest solution I could think of, was to make it possible to choose a custom startup
page per user in the backend. I added that in two forms:
- My Account (under *System | My Account*)
- User Edit (under *System | Permissions | User*)

Unfortunately I had to add two block rewrites to alter the forms. But hey, who else
modifies these forms? :)