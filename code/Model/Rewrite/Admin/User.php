<?php

class Mgcs_AdminStartup_Model_Rewrite_Admin_User extends Mage_Admin_Model_User
{

    public function getStartupPageUrl()
    {
        if ($this->getAdminstartupPage() > '') {
            $startupPage = $this->getAdminstartupPage();
            $aclResource = 'admin/' . $startupPage;
            if (Mage::getSingleton('admin/session')->isAllowed($aclResource)) {
                $nodePath = 'menu/' . join('/children/', explode('/', $startupPage)) . '/action';
                $url = Mage::getSingleton('admin/config')->getAdminhtmlConfig()->getNode($nodePath);
                if ($url) {
                    return $url;
                }
            }
        }
        return parent::getStartupPageUrl();
    }

}