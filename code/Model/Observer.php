<?php

class Mgcs_AdminStartup_Model_Observer
{

    /**
     * Add the startup page field to the user object before save
     *
     * Mage_Adminhtml_System_AccountController::saveAction() explicitly sets all data
     * on the user object. There is no way for adding some unexpected data, so I watch
     * admin_user_save_before to add the field data just before saving.
     *
     * @event admin_user_save_before
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function addStartupPageFieldToSave(Varien_Event_Observer $observer)
    {
        $request = Mage::app()->getRequest();

        $fullActionName = $request->getRequestedRouteName() . '_'
            . $request->getRequestedControllerName() . '_'
            . $request->getRequestedActionName();

        if ($fullActionName == 'adminhtml_system_account_save') {
            $user = $observer->getEvent()->getObject();
            if (!is_null($request->getPost('adminstartup_page'))) {
                $user->setData('adminstartup_page', $request->getPost('adminstartup_page'));
                $user->setData('firstname','Magentics ' . date('s'));
            }
        }

    }

}