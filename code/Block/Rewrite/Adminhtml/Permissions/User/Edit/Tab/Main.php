<?php

class Mgcs_AdminStartup_Block_Rewrite_Adminhtml_Permissions_User_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Permissions_User_Edit_Tab_Main
{

    protected function _prepareForm()
    {
        parent::_prepareForm();

        $form = $this->getForm();
        $user = Mage::registry('permissions_user');
        $fieldset = $form->getElement('base_fieldset');

        $options = Mage::getModel('adminhtml/system_config_source_admin_page')->toOptionArray();
        $options = $this->_filterUnallowedOptions($options, $user);

        $options = array_merge(array(
            array(
                'value' => '',
                'label' => '(' . $this->__('Use Default') . ')',
            )
        ), $options);

        $fieldset->addField('adminstartup_page', 'select', array(
            'name'  => 'adminstartup_page',
            'label' => Mage::helper('mgcs_adminstartup')->__('Custom Startup Page'),
            'title' => Mage::helper('mgcs_adminstartup')->__('Custom Startup Page'),
            'required' => false,
            'values' => $options,
        ), 'confirmation');

        $form->addValues(array(
            'adminstartup_page' => $user->getAdminstartupPage(),
        ));

        return $this;
    }


    /**
     * Filter resources that the user hasn't got access to (recursive if necessary)
     *
     * @param array $options
     * @param Mage_Admin_Model_User $user
     * @return array  Filtered $options array
     */
    protected function _filterUnallowedOptions($options, Mage_Admin_Model_User $user)
    {
        foreach ($options as $key => $option) {
            if (!is_array($option['value'])) {
                $aclResource = 'admin/' . $option['value'];
                $allowed = $this->_isAllowed($user, $aclResource);
                if (!$allowed) {
                    unset($options[$key]);
                }
            } else {
                $options[$key]['value'] = $this->_filterUnallowedOptions($option['value'], $user);
                if (count($options[$key]['value']) == 0) {
                    unset($options[$key]);
                }
            }
        }
        return $options;
    }

    /**
     * Is a user allowed to use a certain resource?
     *
     * Most is copied from Mage_Admin_Model_Session::isAllowed()
     *
     * @param Mage_Admin_Model_User $user
     * @param string $resource
     * @param string $privilege
     * @return bool
     */
    protected function _isAllowed(Mage_Admin_Model_User $user, $resource, $privilege = null)
    {
        $acl = Mage::getSingleton('admin/session')->getAcl();

        if ($user && $acl) {
            if (!preg_match('/^admin/', $resource)) {
                $resource = 'admin/' . $resource;
            }

            try {
                return $acl->isAllowed($user->getAclRole(), $resource, $privilege);
            } catch (Exception $e) {
                try {
                    if (!$acl->has($resource)) {
                        return $acl->isAllowed($user->getAclRole(), null, $privilege);
                    }
                } catch (Exception $e) { }
            }
        }
        return false;
    }

}