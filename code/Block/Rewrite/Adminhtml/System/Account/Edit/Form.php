<?php

class Mgcs_AdminStartup_Block_Rewrite_Adminhtml_System_Account_Edit_Form
    extends Mage_Adminhtml_Block_System_Account_Edit_Form
{

    protected function _prepareForm()
    {
        parent::_prepareForm();

        $form = $this->getForm();
        $fieldset = $form->getElement('base_fieldset');

        $options = Mage::getModel('adminhtml/system_config_source_admin_page')->toOptionArray();
        $options = $this->_filterUnallowedOptions($options);

        $options = array_merge(array(
            array(
                'value' => '',
                'label' => '(' . $this->__('Use Default') . ')',
            )
        ), $options);

        $fieldset->addField('adminstartup_page', 'select',
            array(
                'name'  => 'adminstartup_page',
                'label' => Mage::helper('mgcs_adminstartup')->__('Custom Startup Page'),
                'title' => Mage::helper('mgcs_adminstartup')->__('Custom Startup Page'),
                'required' => false,
                'values' => $options,
            )
        );

        $userId = Mage::getSingleton('admin/session')->getUser()->getId();
        $user = Mage::getModel('admin/user')
            ->load($userId);
        $form->addValues(array(
            'adminstartup_page' => $user->getAdminstartupPage(),
        ));

        return $this;
    }

    /**
     * Filter resources that the user hasn't got access to (recursive if necessary)
     *
     * @param array $options
     * @param Mage_Admin_Model_User $user
     * @return array  Filtered $options array
     */
    protected function _filterUnallowedOptions($options)
    {
        foreach ($options as $key => $option) {
            if (!is_array($option['value'])) {
                $aclResource = 'admin/' . $option['value'];
                if (!Mage::getSingleton('admin/session')->isAllowed($aclResource)) {
                    unset($options[$key]);
                }
            } else {
                $options[$key]['value'] = $this->_filterUnallowedOptions($option['value']);
                if (count($options[$key]['value']) == 0) {
                    unset($options[$key]);
                }
            }
        }
        return $options;
    }

}