<?php

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// Add reset password link token column
$installer->getConnection()->addColumn($installer->getTable('admin/user'), 'adminstartup_page', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'   => 250,
    'nullable' => true,
    'default'  => null,
    'comment'  => 'Custom Admin Startup Page'
));

$installer->endSetup();
